import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Output() loggedIn: EventEmitter<string> = new EventEmitter();

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(1)])
  });

  constructor() { }

  ngOnInit(): void {
  }

  get username() {
    return this.registerForm.get(['username']);
  }

  onRegisterClicked() {
    this.loggedIn.emit(this.registerForm.value.username);
  }
}
