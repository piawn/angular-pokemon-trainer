import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TrainerCollectionService } from 'src/app/services/trainer-collection/trainer-collection.service';

@Component({
  selector: 'app-site-header',
  templateUrl: './site-header.component.html',
  styleUrls: ['./site-header.component.css']
})
export class SiteHeaderComponent implements OnInit {
  public trainerName: string;
  public loggedIn: boolean;

  constructor(private session: SessionService, private trainerCollection: TrainerCollectionService, private router: Router) {
    this.session.isUserLoggedIn$.subscribe(value => {
      this.loggedIn = value;
      this.getTrainerName();
    });
  }

  ngOnInit(): void {
  }

  getTrainerName() {
    this.trainerName = this.session.get();
  }

  onProfileClicked() {
    this.router.navigateByUrl('/trainer');
  }

  onTitleClicked() {
    this.router.navigateByUrl('/catalogue');
  }

  onLogoutClicked() {
    this.session.removeUser();
    this.trainerCollection.removeCollection();
    this.router.navigateByUrl('/landing');
  }
}
