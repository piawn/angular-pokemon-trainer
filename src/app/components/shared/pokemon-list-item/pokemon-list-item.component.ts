import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Pokemon } from '../../../models/pokemon.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {

  @Input() pokemon: Pokemon;
  @Output() pokemonClicked: EventEmitter<Pokemon> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onCardClicked() {
    this.pokemonClicked.emit(this.pokemon);
  }

}
