import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { TrainerCollectionService } from 'src/app/services/trainer-collection/trainer-collection.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  private pokemonName: string = '';
  public pokemon: Pokemon;

  constructor(private pokemonService: PokemonService,
    private trainerCollection: TrainerCollectionService,
    private route: ActivatedRoute) {
    this.pokemonName = String(this.route.snapshot.params.id);
  }

  async ngOnInit() {

    try {
      this.pokemon = await this.pokemonService.getPokemonById(this.pokemonName);
    } catch (e) {
      console.error(e);
    }
  }

  get pokemonInCollection(): boolean {
    return this.trainerCollection.isPokemonInCollection(this.pokemon.id)
  }

  onCollectPokemonClicked() {
    this.trainerCollection.addToCollection(this.pokemon);
  }

  onRemovePokemonClicked() {
    this.trainerCollection.removePokemonFromCollection(this.pokemon.id);

  }
}
