import { Component, OnInit, Input } from '@angular/core';
import { PokemonService } from '../../../services/pokemon/pokemon.service';
import { Pokemon } from '../../../models/pokemon.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  @Input() pokemons: Pokemon;

  public pokemonList: Pokemon[] = [];

  constructor(private pokemonService: PokemonService, private router: Router) { }

  async ngOnInit() {

    try {
      this.pokemons = await this.pokemonService.getPokemons();
    } catch (e) {
      console.error(e);
    }
  }

  handlePokemonClicked(pokemon: Pokemon) {
    this.router.navigateByUrl(`/catalogue/${pokemon.name}`);
  }
}
