import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../../models/pokemon.model';
import { TrainerCollectionService } from 'src/app/services/trainer-collection/trainer-collection.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {
  public collectedPokemons: Pokemon[] = [];

  constructor(private trainerCollection: TrainerCollectionService, private router: Router) { }

  ngOnInit(): void {
    this.getCollectedPokemon();
  }

  getCollectedPokemon() {
    this.collectedPokemons = this.trainerCollection.getCollection();
  }

  handlePokemonClicked(pokemon: Pokemon) {
    this.router.navigateByUrl(`/catalogue/${pokemon.name}`);
  }
}
