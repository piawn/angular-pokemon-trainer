import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private session: SessionService, private router: Router) {
    if (this.session.get() !== '') {
      this.router.navigateByUrl('/catalogue');
    }
  }

  ngOnInit(): void {
  }

  handleLoginClicked(username: string) {
    this.session.save(username);
    this.router.navigateByUrl('/catalogue');
    this.session.isUserLoggedIn$.next(true);
  }
}
