import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SiteHeaderComponent } from './components/shared/site-header/site-header.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { RegisterFormComponent } from './components/forms/register-form/register-form.component';
import { PokemonListItemComponent } from './components/shared/pokemon-list-item/pokemon-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    SiteHeaderComponent,
    LandingComponent,
    TrainerComponent,
    CatalogueComponent,
    DetailComponent,
    RegisterFormComponent,
    PokemonListItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
