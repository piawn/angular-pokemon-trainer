import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './components/pages/landing/landing.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogue/:id',
    component: DetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/landing'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
