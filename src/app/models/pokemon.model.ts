import { Ability } from './ability.model'
import { Move } from './move.model';
import { Stats } from './stats.model'
import { Type } from './type.model';

export interface Pokemon {
    name: string,
    imageUrl: string,
    id?: number,
    image?: string,
    types?: Type[],
    stats?: Stats[],
    height?: number,
    weight?: number,
    abilities?: Ability[],
    base_experience?: number,
    moves?: Move[]
}