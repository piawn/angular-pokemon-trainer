import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public isUserLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
    if (this.get() !== '') {
      this.isUserLoggedIn$.next(true);
    }
  }
  save(user: string) {
    localStorage.setItem('user', user);
  }

  get(): string {
    return localStorage.getItem('user') || '';
  }

  removeUser() {
    localStorage.removeItem('user');
    this.isUserLoggedIn$.next(false);
  }
}
