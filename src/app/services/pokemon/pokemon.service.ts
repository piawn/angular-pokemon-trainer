import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment';
import { Pokemon } from '../../models/pokemon.model';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) {
  }

  getPokemons() {
    return this.http.get<Pokemon[]>(`${environment.apiUrl}/pokemon`)
      .pipe(map((data: any) => data.results.map(pokemon => {
        return {
          ...pokemon,
          ...this.getIdAndImage(pokemon.url)
        }
      }) || []),
        catchError(error => { return throwError(error.message) })).toPromise();
  }

  private getIdAndImage(url: string): any {
    const id = url.split('/').filter(Boolean).pop();
    return { id, imageUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png` };
  }

  private getImage(id: number): any {
    return { imageUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png` };
  }

  getPokemonById(name: string) {
    return this.http.get<Pokemon>(`${environment.apiUrl}/pokemon/${name}`).toPromise()
      .then((pokemon: Pokemon) => {
        return {
          ...pokemon,
          ...this.getImage(pokemon.id)
        }
      });
  }
}

