import { Injectable } from '@angular/core';
import { Pokemon } from '../../models/pokemon.model';

const STORAGE_KEY = 'pokemon_collection';

@Injectable({
  providedIn: 'root'
})
export class TrainerCollectionService {

  public currentCollection: Pokemon[] = [];

  constructor() { }

  addToCollection(pokemon: Pokemon): boolean {
    this.currentCollection = [...this.getCollection()];
    if (this.isPokemonInCollection(pokemon.id)) {
      return false;
    }
    this.currentCollection.push(pokemon);
    const json = JSON.stringify(this.currentCollection);
    localStorage.setItem(STORAGE_KEY, json);
    return true;
  }

  getCollection(): Pokemon[] {
    const storedValue = localStorage.getItem(STORAGE_KEY);
    if (storedValue) {
      return JSON.parse(storedValue)
    }
    return this.currentCollection;
  }

  isPokemonInCollection(id: number): boolean {
    this.currentCollection = this.getCollection();
    return this.currentCollection.some((pokemon: Pokemon) => {
      return pokemon.id === id;
    })
  }

  removePokemonFromCollection(removeId: number) {
    this.currentCollection = this.getCollection();
    let updatedCollection = this.currentCollection.filter((pokemon: Pokemon) => pokemon.id !== removeId)
    localStorage.setItem(STORAGE_KEY, JSON.stringify(updatedCollection));
  };

  removeCollection(): void {
    localStorage.removeItem(STORAGE_KEY);
  }
}